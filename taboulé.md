# Taboulé

## Zutaten

für 8 Personen

| **Menge** | **Zutat** |
| --- | --- |
| 500g | Bulgur |
| 4 Bund | glatte Petersilie |
| 1 Bund | Minze |
| 4 große | Tomaten |
| 1 Stück | Salatgurke |
| 2 Stück | Zwiebeln, rot |
| 4 Stück | Zitronen (Saft, mit etwas Fruchtfleisch) |
| n.B. | Salz |
| n.B. | Olivenöl |


## Zubereitung

1. Bulgur in gleicher Menge kochendem Wasser übergießen und 30 Minuten quellen lassen
1. Kräuter fein hacken (ohne Stile)
1. Gemüse putzen und in kleine Würfel schneiden
1. Alles in einer großen Schüssel vermengen
1. Mit Salz und Zitronensaft abschmecken
