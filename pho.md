Serves 4 to 6
What You Need
Ingredients

    For the quick broth:
    2

    large onions
    4-inch piece

    fresh ginger
    2 (3-inch)

    whole cinnamon sticks
    2

    whole star anise
    3

    whole cloves
    2 teaspoons

    whole coriander seeds
    6 cups

    low-sodium beef broth
    1 tablespoon

    soy sauce (substitute tamari if making gluten-free)
    1 tablespoon

    fish sauce
    3

    carrots, peeled and roughly chopped

    To serve:
    1/2 pound

    sirloin steak, round eye, or London broil
    8 ounces

    dried rice noodles (bahn pho, 1/16-, 1/8-, or 1/-4 inch wide)
    3

    scallions
    1

    chili pepper (Thai bird, serrano, or jalapeño)
    1 to 2

    limes
    1 cup

    bean sprouts
    1 cup

    fresh herbs (cilantro, basil, Thai basil, mint, or a mix)

    Hot sauce, Sriracha, or hoisin sauce, to serve 

    Equipment

    Tongs

    Baking sheet

    2-quart (or larger) saucepan

    Measuring cups and spoons

    Chef's knife

    Second saucepan for cooking the noodles

Instructions

    Prepare the onions and ginger: Peel the onions and cut them into quarters through the root. Peel the ginger and slice it into quarters down its length.

    Char the onions and ginger: Using tongs, char the onions and ginger on all sides over high flame on a gas stove, or on a baking sheet placed directly under the broiler (about 5 minutes on each side) — until the onions and ginger pieces show charred spots. Rinse the pieces under cool water to remove any loose, gritty, overly charred bits.

    Dry-roast the spices: Place the cinnamon, star anise, cloves, and coriander seeds in the bottom of a dry 2-quart saucepan and dry-roast over medium-low heat for 1 to 2 minutes, until toasty and very fragrant. Stir frequently to prevent the spices from scorching.

    Combine the broth ingredients: To the pan with the spices, add the broth, soy sauce, fish sauce, chopped carrots, and the charred onions and ginger.

    Cover and simmer the broth: Bring the broth to a boil over medium-high heat, then reduce the heat to medium-low. Cover and continue simmering for 30 minutes to give time for all the spices and aromatics to infuse in the broth.

    Freeze the beef for 15 minutes: While the broth is simmering, put the beef on a plate, cover with plastic wrap, and freeze for 15 minutes. The edges of the beef should feel firm to the touch, but the beef should not be frozen through. This will make it easier to slice the beef thinly.

    Slice the beef into thin slices: Remove the beef from the freezer and immediately use your sharpest knife to slice the beef into very thin slices. Slice across the grain, and aim for slices no thicker than 1/4-inch. Once sliced, keep the beef covered and refrigerated until ready to serve.

    Cook the rice noodles: Bring a second saucepan of water to a boil, drop in the rice noodles and cook according to package instructions (typically 1 minute for very thin noodles and up to 4 minutes for wider noodles). Strain the noodles and run them under cool water to stop cooking. The noodles will start to stick together after cooking, so either divide them immediately between serving bowls or toss them with a little neutral-tasting oil to prevent sticking.

    Prepare the rest of the pho toppings: Thinly slice the scallions and the chili pepper. Cut the lime into wedges. Place the bean sprouts in a serving dish. Roughly chop the herbs or tear them with your hands. Arrange all the toppings on a serving dish and place it on the table.

    Strain the broth: When the broth is ready, set a strainer over another bowl or saucepan, and strain the solids from the broth. Discard the solids. Place the broth back over low heat and keep it just below a simmer — you should see a fair amount of steam, but the broth should not be boiling. The broth needs to be quite hot to cook the beef.

    Prepare the pho bowls: If you haven't already done so, divide the noodles between serving bowls and top with a few slices of raw beef. Arrange the beef in a single layer so that the slices will cook evenly in the broth; slices that are stacked or clumped may not cook all the way through.

    Ladle the hot broth over top: Ladle the steaming broth into each bowl, pouring it evenly over the beef in order to cook it. The beef should immediately start to turn opaque. Fill each bowl with as much broth as desired.

    Serve the pho with toppings: Serve the pho at the table and let each person top their bowl as they like.

Recipe Notes

    Vegetarian pho: Use vegetable stock or broth and skip the fish sauce. Instead of slices of beef, top the pho with tofu, seitan, mushrooms, bok choy, broccoli, or other vegetables. See here for a full recipe: Vegetarian Pho.
    Make-ahead moments: The broth can be prepared and kept refrigerated for 5 days or frozen for up to 3 months. The beef can be sliced and kept refrigerated for several hours (no longer than 24 hours). The noodles can be prepared, tossed with a bit of neutral-tasting oil, and kept refrigerated for up to a day before serving. The toppings can also be prepped up to a day ahead and kept refrigerated until serving.
    Storing leftovers: Leftover noodles stored in broth will ultimately absorb all the broth and become gummy. If you have leftovers, store the noodles, the broth, the beef, and the toppings in separate containers. Raw slices of beef will keep for a day or two; they can also be quickly cooked in hot broth and then kept refrigerated for up to 5 days. When reheating, assemble the noodles, beef, and broth in a bowl and microwave; top with garnishes before serving.
