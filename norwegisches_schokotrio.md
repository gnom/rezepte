# Norwegische Schokotrio

## Zutaten

| **Menge** | **Zutat** |
| --- | --- |
| 225g | Weizenmehl (Typ 550) |
| 75g | Hafermehl (Vollkorn) |
| 150g | Butter (Zimmertemperatur) |
| 150g | Zucker |
| 2Tl | Backpulver |
| 1/2 Tl | Salz |
| 1 | Ei |
| 40g | Rübensirup |
| 100g | Vollmilchschokolade |
| 200g | Bitterschokolade |
| 100g | weiße Schokolade |
  
## Zubereitung
  
1. Schokolade grob hacken
1. Alle Zutaten, außer der Schokolade, zu einem homogenen Teig verarbeiten
1. Schokolade in den Teig einarbeiten
1. Teig zu einer 5cm dicken Rolle formen
1. Teig ca. 30 Minuten im Kühlschrank "fest" werden lassen.
1. Backofen auf 200°C vorheizen
1. Teig in 1cm dicke Scheiben schneiden
1. Teigscheiben mit VIEL Abstand auf ein Blech mit Backpapier legen
1. Ofen auf 175°C zurück drehen und Kekse ca. 15 Minuten backen. Die Kekse müssen noch weich sein, wenn sie den Ofen verlassen. Den "Knack" erhalten sie erst beim Abkühlen. Vorsicht: Die warmen, weichen Kekse zerbrechen sehr leicht!