# Die besten veganen Mac & Cheese

Probier' doch mal unsere superschnell gekochten und genial leckeren veganen Mac & Cheese mit einer unglaublich cremig-käsigen Sauce aus Kartoffeln, Karotten, Cashewkernen, Knoblauch und Hefeflocken! Dazu packen wir Brokkoli für die Extraportion Gemüse. Oh yeah! 🥦

# Zutaten für 2 Portionen
## FÜR DIE SAUCE
* 200 g Kartoffel
* 1 Karotte ca. 60 g
* 2 Knoblauchzehen
* 1 EL Tapiokastärke
* 80 g Cashewkerne eingeweicht
* 4 EL Hefeflocken
* Saft von 1/2 Zitrone
* 1 EL mittelscharfer Senf
* 1 Prise Kurkuma
* 1 EL Olivenöl
* 1 1/2 TL Salz
* 200 ml Hafermilch
* 100 ml Kochwasser

## AUSSERDEM
* 150 g Brokkoli
* 250 g Nudeln

# Zubereitung
1. Kartoffel und Karotte in kleine Würfeln schneiden und gar kochen. 100 ml Kochwasser auffangen und beiseite stellen.
1. Alle Zutaten für die Käsesauce in einen High-Speed-Blender oder Mixer geben und fein pürieren. Brokkoli in kleine Röschen teilen.
1. Nudeln in reichlich Salzwasser 8 Minuten kochen. Nach 3 Minuten Brokkoliröschen dazugeben und anschließend alles in ein Sieb abgießen.
1. Käsesauce in den Nudeltopf geben, aufkochen und 1-2 Minuten bei niedriger bis mittlerer Hitze köcheln lassen. Nudeln und Brokkoli mit in die Sauce geben und gut verrühren.